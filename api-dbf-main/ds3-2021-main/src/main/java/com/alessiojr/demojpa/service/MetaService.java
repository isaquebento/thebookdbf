package com.alessiojr.demojpa.service;

import com.alessiojr.demojpa.domain.Meta;
import com.alessiojr.demojpa.repository.MetaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MetaService {

    private final Logger log = LoggerFactory.getLogger(MetaService.class);

    private final MetaRepository metaRepository;

    public MetaService(MetaRepository metaRepository) {
        this.metaRepository = metaRepository;
    }

    public List<Meta> findAllList(){
        log.debug("Request to get All Meta");
        return metaRepository.findAll();
    }

    public Optional<Meta> findOne(Long id) {
        log.debug("Request to get Meta : {}", id);
        return metaRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Meta : {}", id);
        metaRepository.deleteById(id);
    }

    public Meta save(Meta meta) {
        log.debug("Request to save Meta : {}", meta);
        meta = metaRepository.save(meta);
        return meta;
    }

    public List<Meta> saveAll(List<Meta> metas) {
        log.debug("Request to save Meta : {}", metas);
        metas = metaRepository.saveAll(metas);
        return metas;
    }
}
